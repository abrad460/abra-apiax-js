# ABRA REST javascript Client

JS Klient pro komunikaci s ABRA REST API

## Instalace

Do projektu přidat referenci na soubor **`abraRestClient.js`**.

## Použití

Nastavením `Abra.Rest.IConnection` se definují základní informace o připojení a oprávnění. 

| Položka | Typ | Povinný | Popis |
| ------- | --- | ------- | ----- |
| username | string | Ne | Přihlašovací údaje |
| password | string | Ne | Přihlašovací údaje |
| authorization | AuthorizationKind | Ne | Typ přihlášení, hlavička požadavku |
| authorizationCustomHeader | string | Ne | Obsah vlastní `Authorization` hlavičky |
| connectorUrl | string | Ano | Connector ABRA aplikace |

### authorization

#### Abra.Rest.AuthorizationKind.Basic

    Authorization: Basic [base64(username:password)]

#### Abra.Rest.AuthorizationKind.Custom

Do požadavku bude zadaná hlavička z property `authorizationCustomHeader`

    Authorization: [authorizationCustomHeader]

**Příklad definice connection:**

```typescript
var connection = <Abra.Rest.IConnection>{
    username: "admin",
    password: "heslo",
    authorization: Abra.Rest.AuthorizationKind.Basic,
    connectorUrl: "https://portal.abra.eu/api/rest/testapi:data"
};
```

Vytvořené connection se předá do konstruktoru při vytvoření instance klienta `Abra.Rest.Client`.

```typescript
var client = new Abra.Rest.Client(connection);
```

### GET

```typescript
get<T>(route: string, param?: ParamComposer)
```

#### route

Pro získání základní části route slouží metoda `getRoute(resource: string)`.

```typescript
var route = client.getRoute("firms");
```

Získané url

[https://portal.abra.eu/api/rest/testapi:data/firms](https://portal.abra.eu/api/rest/testapi:data/firms)

se předá jako parametr do `get` metody.

#### param - nepovinný

Dodatečné parametry dotazu (viz. ParamComposer)

## ParamComposer

`ParamComposer` slouží jako helper ke skládání parametrů  dotazu.

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
    .withExpand(["electronicaddress_id", "firmoffices"])
    .withExpandBo("eu.abra.ns.gx.cmpdef.BOFirmOffice", "address_id")
    .withCount(5)
    .withSkip(101)
    .withOrder(["name", "price"], Abra.Rest.SortDirection.desc);
```

### Filter

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
                new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                new Abra.Rest.FilterItem("price", 10.2, Abra.Rest.FilterOperator.eq)
            ])
        );
```

Vytvořený filtr

```typescript
?filter={"name%23like%23uc":"ABRA*","code%23like%23uc":"2*","price%23eq":10.2}
```

#### Možnost nastavení operátoru `OR`

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
                new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
            ])
        );
```

Vytvořený filtr

```typescript
?filter={"#type":"OR","name%23like%23uc":"ABRA*","number%23lt":10}
```

#### Složený dotaz

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
                new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
            ])
            .withSubCondition(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                    new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                ])
            )
    );
```

Vytvořený filtr

```typescript
?filter={"#type":"OR","name%23like%23uc":"ABRA*","number%23lt":10,"#":{"ID%23eq%23uc":"1000000101","ObjVersion%23lt":1}}
```

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
            .withFilterItems([
                new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
            ])
            .withFilters([new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilters([
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                        ]),
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                        ])
                ])
            ])
        );
```

Vytvořený filtr

```typescript
?filter={"%23type":"OR","name%23like%23uc":"ABRA*","number%23lt":10,"%23":{"%23type":"OR","%23":{"ID%23eq%23uc":"1000000101","ObjVersion%23lt":1},"%23":{"ID%23eq%23uc":"1000000102","ObjVersion%23lt":2}}}
```

#### Aggregační funkce

##### $sum, groupby

Vytvořený filtr

```typescript
$sum?fields=amount,localamount,currency_id.code&groupby=currency_id
```

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFields("amount,localamount,currency_id.code")
        .aggregate.withSum()
        .withGroupBy("currency_id");
```

##### $count

Vytvořený filtr

```typescript
$count?filter={"name%23like%23uc":"ABRA*"}
```

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter()
            .withFilterItems([new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like)]))
        .aggregate.withCount();
```

#### Dotaz na předchůdce

Pokud se udělá "Zásadní oprava" tak v ABRA gen vznika nový záznam a předchozí záznam má odkaz na nově vzniklý záznam.
Do dotazu je potřeba přidat podmínku např `":type":"OR","firm_id": "Y4I0100101", "firm_id.firm_id":"Y4I0100101"`.

Vytvořený filtr

```typescript
{":type":"OR","firm_id": "Y4I0100101", "firm_id.firm_id":"Y4I0100101"}
```

Příklad

```typescript
var composer = new Abra.Rest.ParamComposer()
        .withFilter(new Abra.Rest.Filter()
            .withFilterItems([
                new Abra.Rest.FilterItem(":type", "OR", Abra.Rest.FilterOperator.none),
                new Abra.Rest.FilterItem("firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
                new Abra.Rest.FilterItem("firm_id.firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none)
            ])
        );
```