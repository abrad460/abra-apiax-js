var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var Aggregate = (function () {
            /**
             * Constructor
             */
            function Aggregate(paramComposer) {
                this.paramComposer = paramComposer;
            }
            /**
             * Set $count aggregate function. Default value is 'true'.
             */
            Aggregate.prototype.withCount = function (isCount) {
                this.isCount = isCount || true;
                return this.paramComposer;
            };
            /**
             * Set $sum aggregate function. Default value is 'true'.
             */
            Aggregate.prototype.withSum = function (isSum) {
                this.isSum = isSum || true;
                return this.paramComposer;
            };
            /**
             * Compose parameters to string
             */
            Aggregate.prototype.compose = function () {
                var data = "";
                if (this.isCount) {
                    data = data + "$count";
                }
                if (this.isSum) {
                    data = data + "$sum";
                }
                return data;
            };
            return Aggregate;
        })();
        Rest.Aggregate = Aggregate;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (AuthorizationKind) {
            AuthorizationKind[AuthorizationKind["Basic"] = 0] = "Basic";
            AuthorizationKind[AuthorizationKind["None"] = 1] = "None";
            AuthorizationKind[AuthorizationKind["Custom"] = 2] = "Custom";
        })(Rest.AuthorizationKind || (Rest.AuthorizationKind = {}));
        var AuthorizationKind = Rest.AuthorizationKind;
        var Connection = (function () {
            function Connection() {
                this.authorization = AuthorizationKind.None;
            }
            return Connection;
        })();
        Rest.Connection = Connection;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="connection.ts" />
/// <reference path="responseEval.ts" />
/**
 * Abra REST client. Version 0.5.8
 */
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var Client = (function () {
            function Client(connection) {
                if (!connection)
                    throw new Error("Parameter connection cannot be empty.");
                // clone connection
                this.connection = JSON.parse(JSON.stringify(connection));
                if (this.connection.connectorUrl.charAt(this.connection.connectorUrl.length - 1) !== "/") {
                    this.connection.connectorUrl = this.connection.connectorUrl + "/";
                }
            }
            Client.prototype.getRoute = function (resource) {
                if (resource.charAt(resource.length - 1) === '/') {
                    return this.connection.connectorUrl + resource.substr(0, resource.length - 1);
                }
                else {
                    return this.connection.connectorUrl + resource;
                }
            };
            /**
             * GET
             * @param {string} route - uri route
             * @param {ParamComposer} param - query params, optional
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.get = function (route, param) {
                var _this = this;
                var deferred = $.Deferred();
                if (param) {
                    var uri = param.compose();
                    route = (uri.charAt(0) === '?') ? route + uri : route + '/' + uri;
                }
                $.ajax({
                    beforeSend: function (xhr) { return Client.authorizationHeader(xhr, _this.connection); },
                    //xhrFields: {
                    //    withCredentials: true
                    //},
                    url: route,
                    type: 'GET',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            /**
             * POST
             * @param {string} route - uri route
             * @param {any} data
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.post = function (route, data) {
                return this.sendData(route, data, "POST");
            };
            /**
             * POST file data
             * @param {string} route - uri route
             * @param {any} data
             * @return {JQueryDeferred}
             */
            Client.prototype.postFile = function (route, file) {
                var _this = this;
                var deferred = $.Deferred();
                $.ajax({
                    url: route,
                    method: "POST",
                    beforeSend: function (xhr) {
                        Client.authorizationHeader(xhr, _this.connection);
                        xhr.setRequestHeader("Content-Type", file.type);
                    },
                    data: file,
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            /**
             * POST file raw data
             * @param {string} route
             * @param {Uint8Array} byteArray
             * @param {string} contentType
             */
            Client.prototype.postFileRawData = function (route, byteArray, contentType) {
                var _this = this;
                var deferred = $.Deferred();
                $.ajax({
                    url: route,
                    method: "POST",
                    beforeSend: function (xhr) {
                        Client.authorizationHeader(xhr, _this.connection);
                        xhr.setRequestHeader("Content-Type", contentType);
                    },
                    data: byteArray,
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            /**
             * PUT
             * @param {string} route - uri route
             * @param {any} data
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.put = function (route, data) {
                return this.sendData(route, data, "PUT");
            };
            /**
             * DELETE
             * @param {string} route - uri route
             * @param {any} data
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.delete = function (route, data) {
                var _this = this;
                var deferred = $.Deferred();
                $.ajax({
                    beforeSend: function (xhr) { return Client.authorizationHeader(xhr, _this.connection); },
                    url: route,
                    type: 'DELETE',
                    data: JSON.stringify(data || undefined),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            /**
             * Send data (POST, PUT)
             * @param {string} route - uri route
             * @param {ParamComposer} param - query params, optional
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.sendData = function (route, data, method) {
                var _this = this;
                var deferred = $.Deferred();
                $.ajax({
                    url: route,
                    method: method || "POST",
                    beforeSend: function (xhr) { return Client.authorizationHeader(xhr, _this.connection); },
                    data: JSON.stringify(data || {}),
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        deferred.resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        deferred.reject(jqXHR, textStatus, errorThrown);
                    }
                });
                return deferred;
            };
            /**
             * function Eval
             * @param {string[]} data requested functions
             * @return {JQueryDeferred<T>}
             */
            Client.prototype.fnEval = function (data) {
                var route = this.connection.connectorUrl + "$gxeval";
                var requestData = new Array();
                data.forEach(function (d) {
                    requestData.push({ eval: d });
                });
                return this.sendData(route, requestData, "POST");
            };
            Client.prototype.getAuthHeader = function () {
                var _this = this;
                return function (xhr) {
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(_this.connection.username + ":" + _this.connection.password))));
                };
            };
            /**
             * Compose authorization header
             * @param {JQueryXHR} xhr
             * @param {IConnection} connection
             */
            Client.authorizationHeader = function (xhr, connection) {
                switch (connection.authorization) {
                    case Rest.AuthorizationKind.Basic:
                        xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(connection.username + ":" + connection.password))));
                        return;
                    case Rest.AuthorizationKind.Custom:
                        xhr.setRequestHeader("Authorization", connection.authorizationCustomHeader);
                        return;
                    case Rest.AuthorizationKind.None:
                        return;
                    default:
                        return;
                }
            };
            return Client;
        })();
        Rest.Client = Client;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var Filter = (function () {
            /**
             * Constructor
             */
            function Filter(logic) {
                this.logic = Rest.FilterLogic.and;
                this.logic = logic;
                this.items = new Array();
                this.filters = new Array();
            }
            Filter.prototype.withFilterItems = function (filterItems) {
                var _this = this;
                filterItems.map(function (x) { return _this.items.push(x); });
                return this;
            };
            Filter.prototype.withFilters = function (filters) {
                var _this = this;
                filters.map(function (x) { return _this.filters.push(x); });
                return this;
            };
            Filter.prototype.isEmpty = function () {
                var isEmptyFilter = true;
                for (var i = 0; i < this.filters.length; i++) {
                    var isEmpty = this.filters[i].isEmpty();
                    if (!isEmpty) {
                        isEmptyFilter = false;
                        break;
                    }
                }
                return (this.items.length === 0) && isEmptyFilter;
            };
            Filter.prototype.compose = function () {
                if ((!this.items || this.items.length === 0) &&
                    (!this.filters || this.filters.length === 0)) {
                    return null;
                }
                var logic = this.logic === Rest.FilterLogic.or ? "\"" + Rest.ParamComposer.Delimiter + "type\":\"OR\"," : "";
                var filterConditions = this.filters
                    .map(function (x) { return "\"" + Rest.ParamComposer.Delimiter + "\":{" + x.compose() + "}"; })
                    .join(",");
                var items = this.items.map(function (x) { return x.compose(); }).join(",");
                if (filterConditions && filterConditions.length > 0 && items && items.length > 0) {
                    filterConditions = "," + filterConditions;
                }
                return (logic + items + filterConditions);
            };
            return Filter;
        })();
        Rest.Filter = Filter;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var FilterItem = (function () {
            /**
             * Constructor
             * @param {string} key
             * @param {any} value
             * @param {FilterOperator} operator
             * @param {boolean} caseSensitive - default false
             */
            function FilterItem(key, value, operator, caseSensitive) {
                this.key = key;
                this.value = value;
                this.operator = operator;
                this.caseSensitive = caseSensitive || false;
            }
            FilterItem.prototype.compose = function () {
                if (!this.key)
                    return null;
                var value = this.value;
                var upperCaseParam = "";
                // upperCase param format: name%23like%23uc":"ABRAHAM*"}
                if (typeof this.value === "string"
                    && !this.caseSensitive
                    && this.operator !== Rest.FilterOperator.in
                    && this.operator !== Rest.FilterOperator.nin) {
                    value = this.value.toUpperCase();
                    upperCaseParam = Rest.ParamComposer.Delimiter + "uc";
                }
                if (this.operator === Rest.FilterOperator.in || this.operator === Rest.FilterOperator.nin) {
                    if (typeof this.value === "string") {
                        value = '["' + this.value + '"]';
                    }
                    else if (typeof this.value === "number") {
                        value = '[' + this.value + ']';
                    }
                    else {
                        var v = value.reduce(function (prev, current) {
                            var separator = (prev.length > 0) ? ',' : '';
                            return prev + separator + '"' + current + '"';
                        }, "");
                        value = '[' + v + ']';
                    }
                }
                else if (typeof this.value === "string") {
                    value = '"' + value + '"';
                }
                if (this.operator === Rest.FilterOperator.none) {
                    return '"' + this.key + '":' + value;
                }
                return '"' + this.key + Rest.ParamComposer.Delimiter + Rest.FilterOperator[this.operator] + upperCaseParam + '":' + value;
            };
            return FilterItem;
        })();
        Rest.FilterItem = FilterItem;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (FilterLogic) {
            // Or
            FilterLogic[FilterLogic["or"] = 0] = "or";
            // And
            FilterLogic[FilterLogic["and"] = 1] = "and";
        })(Rest.FilterLogic || (Rest.FilterLogic = {}));
        var FilterLogic = Rest.FilterLogic;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (FilterOperator) {
            // Not used
            FilterOperator[FilterOperator["none"] = 0] = "none";
            // Equals
            FilterOperator[FilterOperator["eq"] = 1] = "eq";
            // "In" is included in the list. For that type field value must be an array type. Example: {"Code#in":["001","002","003"]} 
            FilterOperator[FilterOperator["in"] = 2] = "in";
            // Like
            FilterOperator[FilterOperator["like"] = 3] = "like";
            // Less than
            FilterOperator[FilterOperator["lt"] = 4] = "lt";
            // Less than or equal
            FilterOperator[FilterOperator["lte"] = 5] = "lte";
            // Greater than
            FilterOperator[FilterOperator["gt"] = 6] = "gt";
            // Greater than or equal
            FilterOperator[FilterOperator["gte"] = 7] = "gte";
            // Greate than or Less than. This is similar as "not equal" 
            FilterOperator[FilterOperator["gl"] = 8] = "gl";
            // "Not in" same as "in" condition, but is negated 
            FilterOperator[FilterOperator["nin"] = 9] = "nin";
            // Contains
            FilterOperator[FilterOperator["cont"] = 10] = "cont";
        })(Rest.FilterOperator || (Rest.FilterOperator = {}));
        var FilterOperator = Rest.FilterOperator;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
/// <reference path="aggregate.ts" />
/// <reference path="filter.ts" />
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var ParamComposer = (function () {
            function ParamComposer() {
                this.fields = new Array();
                this.addfields = new Array();
                this.expand = new Array();
                this.expandBo = new Array();
                this.order = new Array();
                this.groupBy = new Array();
                this.filter = undefined;
                this.aggregate = new Rest.Aggregate(this);
            }
            /**
             * Append fields criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withFields = function (fields) {
                if (typeof fields === 'string') {
                    this.fields.push(fields);
                }
                else {
                    this.fields.push(fields.join(','));
                }
                return this;
            };
            /**
             * Append addfields criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withAddFields = function (fields) {
                if (typeof fields === 'string') {
                    this.addfields.push(fields);
                }
                else {
                    this.addfields.push(fields.join(','));
                }
                return this;
            };
            /**
             * Append expand criteria to expand
             * @param {string | Array<string>} expand - address_id
             */
            ParamComposer.prototype.withExpand = function (expand) {
                if (typeof expand === 'string') {
                    this.expand.push(expand);
                }
                else {
                    this.expand.push(expand.join(','));
                }
                return this;
            };
            /**
             * Append expand criteria to expand (businessObject)
             * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
             * @param {string | Array<string>} expand - address_id
             */
            ParamComposer.prototype.withExpandBo = function (bo, expand) {
                var boDef = "(" + bo + ")";
                if (typeof expand === 'string') {
                    this.expandBo.push([boDef, expand]);
                }
                else {
                    this.expandBo.push([boDef, expand.join(',')]);
                }
                return this;
            };
            /**
             * Append count criteria
             * @param {number} count
             */
            ParamComposer.prototype.withCount = function (count) {
                this.count = count || 0;
                return this;
            };
            /**
             * Append count criteria
             * @param {number} skip
             */
            ParamComposer.prototype.withSkip = function (skip) {
                this.skip = skip || 0;
                return this;
            };
            /**
             * Append order criteria
             * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
             * @param {Abra.Rest.SortDirection} direction - sort direction
             */
            ParamComposer.prototype.withOrder = function (order, direction) {
                if (typeof order === 'string') {
                    this.order.push(order);
                }
                else {
                    this.order.push(order.join(','));
                }
                this.orderKind = direction || Rest.SortDirection.asc;
                return this;
            };
            /**
             * Append filter criteria
             */
            ParamComposer.prototype.withFilter = function (filter) {
                this.filter = filter;
                return this;
            };
            /**
             * Append groupBy criteria
             * @param {Array<string>} fields
             */
            ParamComposer.prototype.withGroupBy = function (fields) {
                if (typeof fields === 'string') {
                    this.groupBy.push(fields);
                }
                else {
                    this.groupBy.push(fields.join(','));
                }
                return this;
            };
            /**
             * Set fulltext
             * @param text
             */
            ParamComposer.prototype.withFulltext = function (text) {
                this.fulltext = text;
                return this;
            };
            /**
             * Compose parameters to a URI (string)
             */
            ParamComposer.prototype.compose = function () {
                var _this = this;
                var data = "?";
                // FIELDS
                if (this.fields.length > 0) {
                    this.fields.map(function (item) {
                        data = _this.addComposeItem(data, function () { return "fields=" + encodeURIComponent(item); });
                    });
                }
                // ADDFIELDS
                if (this.addfields.length > 0) {
                    this.addfields.map(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "addfields=" + encodeURIComponent(item);
                        });
                    });
                }
                // EXPAND
                if (this.expand.length > 0) {
                    var expand = "";
                    var comma = "%2C";
                    this.expand.forEach(function (item) {
                        expand = expand + comma + encodeURIComponent(item);
                    });
                    if (expand !== "") {
                        data = this.addComposeItem(data, function () {
                            return "expandfields=" + expand.substr(comma.length, expand.length - comma.length);
                        });
                    }
                }
                if (this.expandBo.length > 0) {
                    this.expandBo.forEach(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "expandfields" + encodeURIComponent(item[0]) + '=' + encodeURIComponent(item[1]);
                        });
                    });
                }
                // COUNT
                if (this.count && this.count > 0) {
                    data = this.addComposeItem(data, function () { return "count=" + _this.count; });
                }
                // SKIP
                if (this.skip && this.skip > 0) {
                    data = this.addComposeItem(data, function () { return "skip=" + _this.skip; });
                }
                // ORDER
                if (this.order.length > 0) {
                    this.order.map(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "order=" + encodeURIComponent(item + ParamComposer.Delimiter + Rest.SortDirection[_this.orderKind].toUpperCase());
                        });
                    });
                }
                // FILTER
                if (this.filter && (this.filter.items.length > 0 ||
                    this.filter.filters.filter(function (filter) { return !filter.isEmpty(); }).length > 0)) {
                    data = this.addComposeItem(data, function () {
                        return 'filter=' + encodeURIComponent('{' + _this.filter.compose() + '}');
                    });
                }
                // GROUPBY
                if (this.groupBy.length > 0) {
                    this.groupBy.map(function (item) {
                        data = _this.addComposeItem(data, function () {
                            return "groupby=" + encodeURIComponent(item);
                        });
                    });
                }
                // FULLTEXT
                if (this.fulltext && this.fulltext.length > 0) {
                    data = this.addComposeItem(data, function () {
                        return "fulltext=" + encodeURIComponent(_this.fulltext);
                    });
                }
                return this.aggregate.compose() + data;
            };
            // Compose param
            ParamComposer.prototype.addComposeItem = function (data, composePart) {
                if (data.length > 1)
                    data = data + "&";
                return data + composePart();
            };
            ParamComposer.Delimiter = ":";
            return ParamComposer;
        })();
        Rest.ParamComposer = ParamComposer;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        var ResponseItemsHelper = (function () {
            function ResponseItemsHelper(response) {
                this["@count"] = response["@count"];
                this["@nextpage"] = response["@nextpage"];
                this.items = response.items;
            }
            ResponseItemsHelper.prototype.count = function () {
                return this["@count"] || 0;
            };
            ResponseItemsHelper.prototype.nextPage = function () {
                return this["@nextpage"] || false;
            };
            return ResponseItemsHelper;
        })();
        Rest.ResponseItemsHelper = ResponseItemsHelper;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
var Abra;
(function (Abra) {
    var Rest;
    (function (Rest) {
        (function (SortDirection) {
            SortDirection[SortDirection["asc"] = 0] = "asc";
            SortDirection[SortDirection["desc"] = 1] = "desc";
        })(Rest.SortDirection || (Rest.SortDirection = {}));
        var SortDirection = Rest.SortDirection;
    })(Rest = Abra.Rest || (Abra.Rest = {}));
})(Abra || (Abra = {}));
//# sourceMappingURL=abraRestClient.js.map