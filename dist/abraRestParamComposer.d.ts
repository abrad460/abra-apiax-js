declare module Abra.Rest {
    class Aggregate {
        private paramComposer;
        private isCount;
        private isSum;
        /**
         * Constructor
         */
        constructor(paramComposer: ParamComposer);
        /**
         * Set $count aggregate function. Default value is 'true'.
         */
        withCount(isCount?: boolean): ParamComposer;
        /**
         * Set $sum aggregate function. Default value is 'true'.
         */
        withSum(isSum?: boolean): ParamComposer;
        /**
         * Compose parameters to string
         */
        compose(): string;
    }
}
declare module Abra.Rest {
    class Filter {
        logic: FilterLogic;
        items: Array<FilterItem>;
        filters: Array<Filter>;
        /**
         * Constructor
         */
        constructor(logic?: FilterLogic);
        withFilterItems(filterItems: Array<FilterItem>): Filter;
        withFilters(filters: Array<Filter>): Filter;
        isEmpty(): boolean;
        compose(): any;
    }
}
declare module Abra.Rest {
    interface IFilterItem {
        key: string;
        value: any;
        operator: FilterOperator;
    }
    class FilterItem implements IFilterItem {
        key: string;
        value: any;
        operator: FilterOperator;
        caseSensitive: boolean;
        /**
         * Constructor
         * @param {string} key
         * @param {any} value
         * @param {FilterOperator} operator
         * @param {boolean} caseSensitive - default false
         */
        constructor(key: string, value: any, operator: FilterOperator, caseSensitive?: boolean);
        compose(): string;
    }
}
declare module Abra.Rest {
    enum FilterLogic {
        or = 0,
        and = 1,
    }
}
declare module Abra.Rest {
    enum FilterOperator {
        none = 0,
        eq = 1,
        in = 2,
        like = 3,
        lt = 4,
        lte = 5,
        gt = 6,
        gte = 7,
        gl = 8,
        nin = 9,
        cont = 10,
    }
}
declare module Abra.Rest {
    class ParamComposer {
        filter: Filter;
        fields: Array<string>;
        addfields: Array<string>;
        expand: Array<string>;
        groupBy: Array<string>;
        expandBo: Array<[string, string]>;
        count: number;
        skip: number;
        order: Array<string>;
        orderKind: SortDirection;
        fulltext: string;
        aggregate: Aggregate;
        static Delimiter: string;
        constructor();
        /**
         * Append fields criteria
         * @param {Array<string>} fields
         */
        withFields(fields: string | Array<string>): ParamComposer;
        /**
         * Append addfields criteria
         * @param {Array<string>} fields
         */
        withAddFields(fields: string | Array<string>): ParamComposer;
        /**
         * Append expand criteria to expand
         * @param {string | Array<string>} expand - address_id
         */
        withExpand(expand: string | Array<string>): ParamComposer;
        /**
         * Append expand criteria to expand (businessObject)
         * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
         * @param {string | Array<string>} expand - address_id
         */
        withExpandBo(bo: string, expand: string | Array<string>): ParamComposer;
        /**
         * Append count criteria
         * @param {number} count
         */
        withCount(count: number): ParamComposer;
        /**
         * Append count criteria
         * @param {number} skip
         */
        withSkip(skip: number): ParamComposer;
        /**
         * Append order criteria
         * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
         * @param {Abra.Rest.SortDirection} direction - sort direction
         */
        withOrder(order: string | Array<string>, direction?: SortDirection): ParamComposer;
        /**
         * Append filter criteria
         */
        withFilter(filter: Filter): ParamComposer;
        /**
         * Append groupBy criteria
         * @param {Array<string>} fields
         */
        withGroupBy(fields: string | Array<string>): ParamComposer;
        /**
         * Set fulltext
         * @param text
         */
        withFulltext(text: string): ParamComposer;
        /**
         * Compose parameters to a URI (string)
         */
        compose(): string;
        private addComposeItem(data, composePart);
    }
}
declare module Abra.Rest {
    interface IResponseEval<T> {
        result: T;
    }
}
declare module Abra.Rest {
    interface IResponseItems<T> {
        "@count": number;
        "@nextpage": boolean;
        items: T;
    }
    class ResponseItemsHelper<T> implements IResponseItems<T> {
        "@count": number;
        "@nextpage": boolean;
        items: T;
        constructor(response: IResponseItems<T>);
        count(): number;
        nextPage(): boolean;
    }
}
declare module Abra.Rest {
    enum SortDirection {
        asc = 0,
        desc = 1,
    }
}
