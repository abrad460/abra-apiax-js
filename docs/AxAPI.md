# Popis Ax API

Základní služba je k dispozici na cestě /rest

Např.: 

    https://portal.abra.eu/api/rest

Kde `https://portal.abra.eu/api` je cesta k nanstalovanému iGATE - takže v Tomcatu kde byl igate.war nainstalován do cesty api.

Služba vrátí seznam aplikací v iGATE s jejich definovanými konektory a informací o připojené databázi.

    [
        {
            "url": "testapi",
            "id": "Y0CGN43WERML3FAN01C5HF1MYC",
            "name": "testapi",
            "title": "testapi",
            "connectors": [
            {
                "name": "data",
                "url": "jdbc:oracle:thin:@192.168.1.11:1521:abra"
            }
            ]
        }
    ]

Je tedy k dispozici jen jedna aplikace s konektorem, který se jmenuje data.

Nyní dotážeme na seznam podporovaných služeb na tomto spojení:

    https://portal.abra.eu/api/rest/testapi:data    
    
    {
    "services": [
    ...
        {
        "url": "pictures",
        "businessObjectClass": "eu.abra.ns.gx.cmpdef.BOPicture",
        "@businessObjectClass": "?class=eu.abra.ns.gx.cmpdef.BOPicture"
        },
    ...
        {
        "url": "firms",
        "businessObjectClass": "eu.abra.ns.gx.cmpdef.BOFirm",
        "@businessObjectClass": "?class=eu.abra.ns.gx.cmpdef.BOFirm"
        },
    ...
    }

u kazde sluby je vidět url a třída business objektu (@businessObjectClass) uz nefunguje - bude odstraněna - funguje to jinak

Nyní se u každé služby mohu dotázat na data (metodou GET) - 

např. firms
    https://portal.abra.eu/api/rest/testapi:data/firms
     
Služba vrátí seznam dat business objektů třídy BOFirm. U každého objektu se ve výchzím stavu vrací všechny "persistentní" položky. Nevrací se vlastněné kolekce a objekty.
Pomocí následujících parametrů je možné žídit jaké položky se budou pro každý objekt vracet:
fields - seznam vracených fieldu - pokud je parametr zadan, prebije vychozi seznam vrazenych polozek
addfields - vychozi seznam, nebo vlastni seznam polozek fields je mozne doplnit o dalsi polozky definovane v tomto parametru
removefields - vychozi seznam, nebo vlastni seznam polozek fields je mozne zkrouhnout o polozky definovane v tomto parametru
expandfields - pomocí tohoto parametru je mozne definovat, ktere odkazove polozky budou rozvinute
parametry fields, addfields, removefields a expandfields je mozne doplnit na fields(trida_business_objektu) a pak urcuji seznam a pripadne rozvinuti polozek pro jine tridy business objektu nez tu zakladni

Vrati firmu s ID = 0290000101 s tim, ze na firme je expadnovana polozka electronicaddress_id a kolekce firmoffices,
v objektu provozovna je expandovana polozka address_id

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101?expandfields=electronicaddress_id,firmoffices&expandfields(eu.abra.ns.gx.cmpdef.BOFirmOffice)=address_id


V nazvech položek je možné používat i tzv. tečkovou konvenci: ResidenceAddress_ID.Street

Zjištění struktury business objektu je možné udělat pomocí $metadata

Např.:

    https://portal.abra.eu/api/rest/testapi:data/firms/$metadata

Data zanořených objektů a kolecí je možné získávat pomocí parametru expandfields nebo pomocí rozvíjení url adresy.

Pokud chceme získat data jednoho business objektu - je třeba do url doplnit jeho ID - např. takto:

    https://portal.abra.eu/api/rest/testapi:data/firms/0001100101

Data tohoto objektu je možné dále rozvíjet pomocí názvů položek kolekcí nebo odkazů:

Vrati kolekci provozoven na firme s ID = 6VK2100101

    https://portal.abra.eu/api/rest/testapi:data/firms/6VK2100101/firmoffices

Vrati data objektu mena na bankovnim ucte s ID = 1000000101

    https://portal.abra.eu/api/rest/testapi:data/bankaccounts/1000000101/currency_id
 
Vrati kolekci radku (pocatky) na pokladne s ID = 1200000101 a vysledne objekty obsahuji pouze polozky beginninglocal,period_id,period_id.name
Vysledne radky jsou serazene podle period_id.name

    https://portal.abra.eu/api/rest/testapi:data/cashdesks/1200000101/rows?fields=beginninglocal,period_id,period_id.name&order=period_id.name

Vrati objekt adresa z provozovny s indexem 0 z firmy s ID = 0290000101

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101/firmoffices%230/address_id

U polozek typu kolekce je moze dotazovat jednotlive business objekty

napr.:

    firmoffices#0 (prvni business objekt)
    firmoffices#3 (ctvrty business objekt)
    firmoffices#first (prvni)
    firmoffices#last (posledni)
    firmoffices#last

Vrati objekt adresa z prvni provozovny z firmy s ID = 0290000101

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101/firmoffices%23first/address_id

Vrati objekt prvni provozovny z firmy s ID = 0290000101

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101/firmoffices%23first

Vrati kolekci provozoven z firmy s ID = 0290000101

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101/firmoffices

Vrati kolekci provozoven z firmy s ID = 0290000101 - polozka address_id je expandovana

    https://portal.abra.eu/api/rest/testapi:data/firms/0290000101/firmoffices?expandfields=address_id


Data vracených seznamů je možné omezovat a třídit:   
Vrati seznam stredisek (max 100 zaznamu) setridenych podle code a zafiltrovanych za code like 2*

    https://portal.abra.eu/api/rest/testapi:data/divisions?count=100&order=code&filter={"code%23like":"2*"}

Vrati seznam firem (max 100 zaznamu) setridenych podle name descending a zafiltrovanych za name like ABRA*

    https://portal.abra.eu/api/rest/testapi:data/firms?count=100&order=name%23DESC&filter={"name%23like":"ABRA*"}

Vrati seznam predkontaci zafiltrovanych za documenttype like 03

    https://portal.abra.eu/api/rest/testapi:data/accpresetdefinitions?filter={"documenttype%23like":"03"}

Další použitelné parametry:

count - maximalni pocet vracenych zaznamu (funguje jen u seznamu - nefunguje u kolekci)
skip - offset  (funguje jen u seznamu)
showclassnames=true - v odpovedi je u každého objektu informace o třídě business objektu
showlinks=true - v odpovedi jsou u polozek typu odkaz (na nevlastnene business objekty) k dispozici položka stejneho jmena s prefixem @ obsahujici cast url, pres kterou je mozne ziskat data daneho business objektu

filter - v tomto parametru se predpoklada JSON pro definici omezeni - stejne fungovani jako v mGATE - bude popsáno detailně + možná i upraveno
oder - seznam polozek, pres ktere se ma tridit - pokud se pozaduje opacne (descending), uvede se #DESC
method - pomoci tohoto parametru je mozne prebit (nastavit) informaci o pozadovane metode
lang - pomoci tohoto parametru je možné řídit jazyk, ve které jsou výstupní informace - jedná se to typicky názvů položek (cs, sk, en - výchozí je cs)

## Reporty:
========
Seznam je možné teké převést do slušného reportu - např. v PDF

Dělá se to doplněním cesty o $report např. takto: 

    https://portal.abra.eu/api/rest/testapi:data/divisions/$report

V reportech je možné používat stejné parametry jako při získání seznamu (vyjma skip a count)

+ je možné použít tyto:

exportkind=ODT - typ exportu (PDF, HTML, ODT, DOCX, PPTX, ODS, XLS, XLSX, CSV, RTF, XML) - výchozí je PDF
pagetype=A3 - velikost stránky (LETTER, A4, A3, A5, ....., B4, B3, ....) - výchozí je A4
portrait=false - zda je stránka portrait nebo landscape - výchozí je true
pagenumberingalse - zda se bude tisknout celkový počet stránek a pořadové číslo stránky - výchozí je true  

## Aggregace

### $sum

    https://portal.abra.eu/igx/rest/issuedinvoices/$sum?fields=amount,localamount,currency_id.code&groupby=currency_id

### $count

    https://portal.abra.eu/igx/rest/firms/$count?filter={"name%23like%23uc":"ABRA*"}

## Čtení a zápis obrázků z Gx:
===========================
Položky, které obsahují data obrázků je buď možné číst spolu s ostatními položkami objektu - pak jsou vracené BASE64, nebo je možné čísta samostatně.
V druhém případě se nevrací JSON odpověď, ale binární data doplněná v odpovědi o Content-Type (např. image/jpeg).

GET
    https://portal.abra.eu/api/rest/testapi:data/pictures/1B70000101/blobdata

Pro zápis je třeba zadat stejnou utl adresu pro POST request

    https://portal.abra.eu/api/rest/testapi:data/pictures/1B70000101/blobdata

do body dát binární data uploadovaného obrázku a do Content-Type nastavit jednu z následujících voleb:
image/jpeg, image/gif, image/png, image/bmp a image/x-icon.
Při uploadu se předaná data a Content-Type převedou na výsledná binární data, která se uloží do položky. 
Odpovědí je krátký JSON s informací o počtu uploadovaných dat.

## Čtení a zápis binárních dat:
============================
Funguje obdobně jako v případě obrázku Gx s tím rozdílem, že se neví a ani nepožaduje Content-Type.
Služba pro vácení binárních dat, vrací Content-Type application/octet-stream a při uploadu na Content-Type nezáleží


    "firmoffices":[{"xxx":1}, {"yyy":2}],
    "firmoffices":{"insert":[], "update":[], "delete":[]},

    {
        "code": "4447",
        "name": "Svoboda Jiří",
        "residenceaddress_id": {
            "city": "Ovčáry u Kolína",
            "street": "Kolínská 121",
            "id": "2ZL0000101",
            "objversion": 5
        },
        "id": "0390000101",
        "objversion": 8
    }

    {
    "code": "4447",
    "name": "Svoboda Jiří",
    "residenceaddress_id": {
        "city": "Ovčáry u Kolína AC",
        "street": "Kolínská 121"
    }
    }

    http://localhost:8081/dev/rest/examples:data/firms/0390000101?showclassnames=true&expandfields=residenceaddress_id&lang=en

POST

    http://localhost:8081/dev/rest/examples:data/firms

nebo
 
    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101
   
    {
        "id":"2GB1000101",
    "firmoffices": [
        {
        "name": "PR XXX",
        "id":"3U71000101"
        },
        {
        "name": "PR YYY",
        "id":"2U71000101"
        }
    ]
    }

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101?expandfields=firmoffices

    {
        "id":"2GB1000101",
    "firmoffices": [
        {
        "name": "PR AAA"
        },
        {
        "name": "PR BBB"
        }
    ]
    }

GET

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/residenceaddress_id

    {
    "recipient": "",
    "city": "",
    "street": "",
    "postcode": "",
    "zip": "",
    "country": "",
    "phonenumber1": "",
    "phonenumber2": "",
    "faxnumber": "",
    "email": "",
    "location": "",
    "countrycode": "",
    "databox": "",
    "gps": "",
    "id": "HD4L000101",
    "objversion": 1
    }

POST 

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/residenceaddress_id

    {
        "city": "Kocourkov",
        "street": "Náhodná -8",
        "postcode": "333 33",
        "country": "Čéééésko"
    }

GET

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/firmoffices%23first

    {
        "parent_id": "2GB1000101",
        "posindex": 2,
        "address_id": "5E4L000101",
        "masscorrespondence": false,
        "name": "PR YYY",
        "synchronizeaddress": false,
        "credit": 0,
        "checkcredit": false,
        "store_id": "0000000000",
        "dealercategory_id": "0000000000",
        "electronicaddress_id": "6E4L000101",
        "officeidentnumber": "",
        "officeunique_id": "2U71000101",
        "id": "2U71000101",
        "objversion": 2
    }

POST

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/firmoffices%23first

    {
        "name": "PR YYY222",
        "synchronizeaddress": true
    }

GET

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/firmoffices

    {
        "firmoffices": [
            {
            "parent_id": "2GB1000101",
            "posindex": 8,
            "address_id": "DF4L000101",
            "masscorrespondence": false,
            "name": "PR AAA",
            "synchronizeaddress": false,
            "credit": 0,
            "checkcredit": false,
            "store_id": "0000000000",
            "dealercategory_id": "0000000000",
            "electronicaddress_id": "EF4L000101",
            "officeidentnumber": "",
            "officeunique_id": "6V71000101",
            "id": "6V71000101",
            "objversion": 1
            },
            {
            "parent_id": "2GB1000101",
            "posindex": 9,
            "address_id": "FF4L000101",
            "masscorrespondence": false,
            "name": "PR BBB",
            "synchronizeaddress": false,
            "credit": 0,
            "checkcredit": false,
            "store_id": "0000000000",
            "dealercategory_id": "0000000000",
            "electronicaddress_id": "GF4L000101",
            "officeidentnumber": "",
            "officeunique_id": "7V71000101",
            "id": "7V71000101",
            "objversion": 1
            }
        ]
    }

POST

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/firmoffices
 
    [
        {
        "name": "PR U1"
        },
        {
        "name": "PR U2",
        "synchronizeaddress": false
        }
    ]

POST

    http://localhost:8081/dev/rest/examples:data/firms/2GB1000101/firmoffices
    
    [
    ]
    
Sice smaže všechny provozovny v dané firmě, ale protože BO firma automaticky zakláda provozovnu, bude ve výsledku obsažena jedna nová provozovna
