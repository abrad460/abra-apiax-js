﻿module Abra.Rest {

    export class Aggregate {

        private paramComposer: ParamComposer;
        private isCount: boolean;
        private isSum: boolean;

        /**
         * Constructor
         */
        constructor(paramComposer: ParamComposer) {
            this.paramComposer = paramComposer;
        }

        /**
         * Set $count aggregate function. Default value is 'true'.
         */
        public withCount(isCount?: boolean) {
            this.isCount = isCount || true;

            return this.paramComposer;
        }

        /**
         * Set $sum aggregate function. Default value is 'true'.
         */
        public withSum(isSum?: boolean) {
            this.isSum = isSum || true;

            return this.paramComposer;
        }

        /**
         * Compose parameters to string
         */
        public compose(): string {
            var data = "";

            if (this.isCount) {
                data = data + "$count";
            }

            if (this.isSum) {
                data = data + "$sum";
            }

            return data;
        }
    }
}