﻿/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="connection.ts" />
/// <reference path="responseEval.ts" />

declare function unescape(s:string): string;

/**
 * Abra REST client. Version 0.5.8
 */

module Abra.Rest {

    export class Client {
        private connection: IConnection;

        constructor(connection: IConnection) {
            if (!connection)
                throw new Error("Parameter connection cannot be empty.");

            // clone connection
            this.connection = <IConnection>JSON.parse(JSON.stringify(connection));
            if (this.connection.connectorUrl.charAt(this.connection.connectorUrl.length - 1) !== "/") {
                this.connection.connectorUrl = this.connection.connectorUrl + "/";
            }
        }

        public getRoute(resource: string) {
            if (resource.charAt(resource.length - 1) === '/') {
                return this.connection.connectorUrl + resource.substr(0, resource.length - 1);
            }
            else {
                return this.connection.connectorUrl + resource;
            }
        }

        /**
         * GET
         * @param {string} route - uri route
         * @param {ParamComposer} param - query params, optional
         * @return {JQueryDeferred<T>}
         */
        public get<T>(route: string, param?: ParamComposer) {
            var deferred = $.Deferred<T>();

            if (param) {
                var uri = param.compose();
                route = (uri.charAt(0) === '?') ? route + uri : route + '/' + uri;
            }

            $.ajax({
                beforeSend: (xhr) => Client.authorizationHeader(xhr, this.connection),
                //xhrFields: {
                //    withCredentials: true
                //},
                url: route,
                type: 'GET',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    deferred.resolve(<T>data);
                },
                error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    deferred.reject(jqXHR, textStatus, errorThrown);
                }
            });

            return deferred;
        }

        /**
         * POST
         * @param {string} route - uri route
         * @param {any} data
         * @return {JQueryDeferred<T>}
         */
        public post<T>(route: string, data: any) {
            return this.sendData<T>(route, data, "POST");
        }

        /**
         * POST file data
         * @param {string} route - uri route
         * @param {any} data
         * @return {JQueryDeferred}
         */
        public postFile(route: string, file: File) {
            var deferred = $.Deferred();

            $.ajax({
                url: route,
                method: "POST",
                beforeSend: (xhr) => {
                    Client.authorizationHeader(xhr, this.connection);
                    xhr.setRequestHeader("Content-Type", file.type)
                },
                data: file,
                processData: false,
                contentType: false,
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    deferred.resolve(data);
                },
                error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    deferred.reject(jqXHR, textStatus, errorThrown);
                }
            });

            return deferred;
        }

        /**
         * POST file raw data
         * @param {string} route
         * @param {Uint8Array} byteArray
         * @param {string} contentType
         */
        public postFileRawData(route: string, byteArray: Uint8Array, contentType: string) {
            var deferred = $.Deferred();

            $.ajax({
                url: route,
                method: "POST",
                beforeSend: (xhr) => {
                    Client.authorizationHeader(xhr, this.connection);
                    xhr.setRequestHeader("Content-Type", contentType);
                },
                data: byteArray,
                processData: false,
                contentType: false,
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    deferred.resolve(data);
                },
                error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    deferred.reject(jqXHR, textStatus, errorThrown);
                }
            });

            return deferred;
        }

        /**
         * PUT
         * @param {string} route - uri route
         * @param {any} data
         * @return {JQueryDeferred<T>}
         */
        public put<T>(route: string, data: any) {
            return this.sendData<T>(route, data, "PUT");
        }

        /**
         * DELETE
         * @param {string} route - uri route
         * @param {any} data
         * @return {JQueryDeferred<T>}
         */
        public delete<T>(route: string, data?: any) {
            var deferred = $.Deferred<T>();

            $.ajax({
                beforeSend: (xhr) => Client.authorizationHeader(xhr, this.connection),
                url: route,
                type: 'DELETE',
                data: JSON.stringify(data || undefined),
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    deferred.resolve(<T>data);
                },
                error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    deferred.reject(jqXHR, textStatus, errorThrown);
                }
            });

            return deferred;
        }

        /**
         * Send data (POST, PUT)
         * @param {string} route - uri route
         * @param {ParamComposer} param - query params, optional
         * @return {JQueryDeferred<T>}
         */
        private sendData<T>(route: string, data: any, method: string) {
            var deferred = $.Deferred<T>();

            $.ajax({
                url: route,
                method: method || "POST",
                beforeSend: (xhr) => Client.authorizationHeader(xhr, this.connection),
                data: JSON.stringify(data || {}),
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    deferred.resolve(<T>data);
                },
                error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    deferred.reject(jqXHR, textStatus, errorThrown);
                }
            });

            return deferred;
        }

        /**
         * function Eval
         * @param {string[]} data requested functions
         * @return {JQueryDeferred<T>}
         */
        public fnEval<T>(data: string[]): JQueryDeferred<IResponseEval<T>[]> {
            var route = this.connection.connectorUrl + "$gxeval";
            var requestData = new Array();

            data.forEach((d) => {
                requestData.push({ eval: d });
            });

            return this.sendData(route, requestData, "POST");
        }

        public getAuthHeader() {
            return (xhr: XMLHttpRequest) => {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(this.connection.username + ":" + this.connection.password))));
            };
        }

        /**
         * Compose authorization header
         * @param {JQueryXHR} xhr
         * @param {IConnection} connection
         */
        private static authorizationHeader(xhr: JQueryXHR, connection: IConnection) {
            switch (connection.authorization) {
                case AuthorizationKind.Basic:
                    xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(connection.username + ":" + connection.password))));
                    return;
                case AuthorizationKind.Custom:
                    xhr.setRequestHeader("Authorization", connection.authorizationCustomHeader);
                    return;
                case AuthorizationKind.None:
                    return;
                default:
                    return;
            }
        }
    }
}