﻿module Abra.Rest {
    export interface IConnection {
        username: string;
        password: string;
        connectorUrl: string;
        authorization: AuthorizationKind;
        authorizationCustomHeader: string;
    }

    export enum AuthorizationKind {
        Basic,
        None,
        Custom
    }

    export class Connection implements Abra.Rest.IConnection {
        public username: string;
        public password: string;
        public connectorUrl: string;
        public authorization: AuthorizationKind;
        public authorizationCustomHeader: string;

        constructor() {
            this.authorization = AuthorizationKind.None;
        }
    }
}