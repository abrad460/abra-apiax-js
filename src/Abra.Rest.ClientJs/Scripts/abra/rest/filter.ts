﻿module Abra.Rest {

    export class Filter {
        public logic = FilterLogic.and;
        public items: Array<FilterItem>;
        public filters: Array<Filter>;

        /**
         * Constructor
         */
        constructor(logic?: FilterLogic) {
            this.logic = logic;
            this.items = new Array<FilterItem>();
            this.filters = new Array<Filter>();
        }

        withFilterItems(filterItems: Array<FilterItem>) {
            filterItems.map(x => this.items.push(x));

            return this;
        }

        withFilters(filters: Array<Filter>) {
            filters.map(x => this.filters.push(x));

            return this;
        }

        isEmpty() {
            var isEmptyFilter = true;
            for (var i = 0; i < this.filters.length; i++) {
                var isEmpty = this.filters[i].isEmpty();
                if (!isEmpty) {
                    isEmptyFilter = false;
                    break;
                }
            }

            return (this.items.length === 0) && isEmptyFilter;
        }

        compose() {
            if ((!this.items || this.items.length === 0) &&
                (!this.filters || this.filters.length === 0)) {
                return null;
            }

            var logic = this.logic === FilterLogic.or ? "\"" + ParamComposer.Delimiter + "type\":\"OR\"," : "";

            var filterConditions = this.filters
                .map(x => "\"" + ParamComposer.Delimiter + "\":{" + x.compose() + "}")
                .join(",");

            var items = this.items.map(x => x.compose()).join(",");
            if (filterConditions && filterConditions.length > 0 && items && items.length > 0) {
                filterConditions = "," + filterConditions;
            }

            return (logic + items + filterConditions);
        }
    }
}