﻿module Abra.Rest {

    export interface IFilterItem {
        key: string,
        value: any,
        operator: FilterOperator
    }

    export class FilterItem implements IFilterItem {
        public key: string;
        public value: any;
        public operator: FilterOperator;
        public caseSensitive: boolean;

        /**
         * Constructor
         * @param {string} key
         * @param {any} value
         * @param {FilterOperator} operator
         * @param {boolean} caseSensitive - default false
         */
        constructor(key: string, value: any, operator: FilterOperator, caseSensitive?: boolean) {
            this.key = key;
            this.value = value;
            this.operator = operator;
            this.caseSensitive = caseSensitive || false;
        }

        public compose() {
            if (!this.key) return null;

            var value = this.value;
            var upperCaseParam = "";

            // upperCase param format: name%23like%23uc":"ABRAHAM*"}
            if (typeof this.value === "string"
                && !this.caseSensitive
                && this.operator !== FilterOperator.in
                && this.operator !== FilterOperator.nin) {
                value = (<string>this.value).toUpperCase();
                upperCaseParam = ParamComposer.Delimiter + "uc";
            }

            if (this.operator === FilterOperator.in || this.operator === FilterOperator.nin) {
                if (typeof this.value === "string") {
                    value = '["' + this.value + '"]';
                } else if (typeof this.value === "number") {
                    value = '[' + this.value + ']';
                } else {
                    var v = (<Array<any>>value).reduce((prev, current) => {
                        var separator = (prev.length > 0) ? ',' : '';
                        return prev + separator + '"' + current + '"';
                    }, "");

                    value = '[' + v + ']';
                }
            }
            else if (typeof this.value === "string") {
                value = '"' + value + '"';
            }

            if (this.operator === FilterOperator.none) {
                return '"' + this.key + '":' + value;
            }

            return '"' + this.key + ParamComposer.Delimiter + FilterOperator[this.operator] + upperCaseParam + '":' + value;
        }
    }
}