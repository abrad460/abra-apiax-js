﻿module Abra.Rest {

    export enum FilterLogic {
        // Or
        or,

        // And
        and
    }
}