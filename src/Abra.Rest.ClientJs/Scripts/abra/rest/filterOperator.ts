﻿module Abra.Rest {

    export enum FilterOperator {
        // Not used
        none,

        // Equals
        eq,

        // "In" is included in the list. For that type field value must be an array type. Example: {"Code#in":["001","002","003"]} 
        in,

        // Like
        like,

        // Less than
        lt,

        // Less than or equal
        lte,

        // Greater than
        gt,

        // Greater than or equal
        gte,

        // Greate than or Less than. This is similar as "not equal" 
        gl,

        // "Not in" same as "in" condition, but is negated 
        nin,

        // Contains
        cont
    }
}