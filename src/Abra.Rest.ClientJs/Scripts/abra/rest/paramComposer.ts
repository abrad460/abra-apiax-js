﻿/// <reference path="aggregate.ts" />
/// <reference path="filter.ts" />

module Abra.Rest {

    export class ParamComposer {
        public filter: Filter;
        public fields: Array<string>;
        public addfields: Array<string>;
        public expand: Array<string>;
        public groupBy: Array<string>;
        public expandBo: Array<[string, string]>;
        public count: number;
        public skip: number;
        public order: Array<string>;
        public orderKind: SortDirection;
        public fulltext: string;

        // aggregate
        aggregate: Aggregate;

        public static Delimiter = ":";

        constructor() {
            this.fields = new Array<string>();
            this.addfields = new Array<string>();
            this.expand = new Array<string>();
            this.expandBo = new Array<[string, string]>();
            this.order = new Array<string>();
            this.groupBy = new Array<string>();
            this.filter = undefined;
            this.aggregate = new Aggregate(this);
        }

        /**
         * Append fields criteria
         * @param {Array<string>} fields
         */
        public withFields(fields: string | Array<string>) {
            if (typeof fields === 'string') {
                this.fields.push(<string>fields);
            }
            else {
                this.fields.push((<Array<string>>fields).join(','));
            }

            return this;
        }

        /**
         * Append addfields criteria
         * @param {Array<string>} fields
         */
        public withAddFields(fields: string | Array<string>) {
            if (typeof fields === 'string') {
                this.addfields.push(<string>fields);
            }
            else {
                this.addfields.push((<Array<string>>fields).join(','));
            }

            return this;
        }

        /**
         * Append expand criteria to expand
         * @param {string | Array<string>} expand - address_id
         */
        public withExpand(expand: string | Array<string>) {
            if (typeof expand === 'string') {
                this.expand.push(<string>expand);
            }
            else {
                this.expand.push((<Array<string>>expand).join(','));
            }

            return this;
        }

        /**
         * Append expand criteria to expand (businessObject)
         * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
         * @param {string | Array<string>} expand - address_id
         */
        public withExpandBo(bo: string, expand: string | Array<string>) {
            var boDef = "(" + bo + ")";
            if (typeof expand === 'string') {
                this.expandBo.push([boDef, <string>expand]);
            }
            else {
                this.expandBo.push([boDef, (<Array<string>>expand).join(',')]);
            }

            return this;
        }

        /**
         * Append count criteria
         * @param {number} count
         */
        public withCount(count: number) {
            this.count = count || 0;

            return this;
        }

        /**
         * Append count criteria
         * @param {number} skip
         */
        public withSkip(skip: number) {
            this.skip = skip || 0;

            return this;
        }

        /**
         * Append order criteria
         * @param {string} bo - eu.abra.ns.gx.cmpdef.BOFirmOffice
         * @param {Abra.Rest.SortDirection} direction - sort direction
         */
        public withOrder(order: string | Array<string>, direction?: SortDirection) {
            if (typeof order === 'string') {
                this.order.push(<string>order);
            }
            else {
                this.order.push((<Array<string>>order).join(','));
            }

            this.orderKind = direction || SortDirection.asc;

            return this;
        }

        /**
         * Append filter criteria
         */
        public withFilter(filter: Filter) {
            this.filter = filter;

            return this;
        }

        /**
         * Append groupBy criteria
         * @param {Array<string>} fields
         */
        public withGroupBy(fields: string | Array<string>) {
            if (typeof fields === 'string') {
                this.groupBy.push(<string>fields);
            }
            else {
                this.groupBy.push((<Array<string>>fields).join(','));
            }

            return this;
        }

        /**
         * Set fulltext
         * @param text
         */
        public withFulltext(text: string) {
            this.fulltext = text;

            return this;
        }

        /**
         * Compose parameters to a URI (string)
         */
        public compose() {
            var data = "?";

            // FIELDS
            if (this.fields.length > 0) {
                this.fields.map(item => {
                    data = this.addComposeItem(data, () => { return "fields=" + encodeURIComponent(item); });
                });
            }

            // ADDFIELDS
            if (this.addfields.length > 0) {
                this.addfields.map(item => {
                    data = this.addComposeItem(data, () => {
                        return "addfields=" + encodeURIComponent(item); 
                    });
                });
            }

            // EXPAND
            if (this.expand.length > 0) {
                var expand = "";
                const comma = "%2C";
                this.expand.forEach(item => {
                    expand = expand + comma + encodeURIComponent(item);
                });
                if (expand !== "") {
                    data = this.addComposeItem(data, () => {
                        return "expandfields=" + expand.substr(comma.length, expand.length - comma.length);
                    });
                }
            }

            if (this.expandBo.length > 0) {
                this.expandBo.forEach(item => {
                    data = this.addComposeItem(data, () => {
                        return "expandfields" + encodeURIComponent(item[0]) + '=' + encodeURIComponent(item[1]);
                    });
                });
            }

            // COUNT
            if (this.count && this.count > 0) {
                data = this.addComposeItem(data, () => { return "count=" + this.count; });
            }

            // SKIP
            if (this.skip && this.skip > 0) {
                data = this.addComposeItem(data, () => { return "skip=" + this.skip; });
            }

            // ORDER
            if (this.order.length > 0) {
                this.order.map(item => {
                    data = this.addComposeItem(data, () => {
                        return "order=" + encodeURIComponent(
                            item + ParamComposer.Delimiter + SortDirection[this.orderKind].toUpperCase()
                        );
                    });
                });
            }

            // FILTER
            if (this.filter && (this.filter.items.length > 0 ||
                this.filter.filters.filter(filter => !filter.isEmpty()).length > 0)) {
                data = this.addComposeItem(data, () => {
                    return 'filter=' + encodeURIComponent('{' + this.filter.compose() + '}');
                });
            }

            // GROUPBY
            if (this.groupBy.length > 0) {
                this.groupBy.map(item => {
                    data = this.addComposeItem(data, () => {
                        return "groupby=" + encodeURIComponent(item);
                    });
                });
            }

            // FULLTEXT
            if (this.fulltext && this.fulltext.length > 0) {
                data = this.addComposeItem(data, () => {
                    return "fulltext=" + encodeURIComponent(this.fulltext);
                });
            }

            return this.aggregate.compose() + data;
        }

        // Compose param
        private addComposeItem(data: string, composePart: () => string) {
            if (data.length > 1) data = data + "&";

            return data + composePart();
        }
    }
}