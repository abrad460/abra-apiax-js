﻿module Abra.Rest {
    export interface IResponseEval<T> {
        result: T;
    }
}