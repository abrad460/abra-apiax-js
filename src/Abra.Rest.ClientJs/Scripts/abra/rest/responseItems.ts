﻿module Abra.Rest {
    export interface IResponseItems<T> {
        "@count": number;
        "@nextpage": boolean;
        items: T;
    }

    export class ResponseItemsHelper<T> implements IResponseItems<T> {

        public "@count": number;
        public "@nextpage": boolean;
        public items: T;

        constructor(response: IResponseItems<T>) {
            this["@count"] = response["@count"];
            this["@nextpage"] = response["@nextpage"];
            this.items = response.items;
        }

        public count() {
            return <number>this["@count"] || 0;
        }

        public nextPage() {
            return <boolean>this["@nextpage"] || false;
        }
    }
}