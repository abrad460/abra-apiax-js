﻿/// <reference path="../typings/jasmine/jasmine.d.ts" />
/// <reference path="../abra/rest/paramcomposer.ts" />
/// <reference path="../abra/rest/connection.ts" />
/// <reference path="../abra/rest/client.ts" />
/// <reference path="../abra/rest/responseItems.ts" />
/// <reference path="../abra/rest/sortDirection.ts" />
/// <reference path="../abra/rest/filterItem.ts" />
/// <reference path="../abra/rest/filterLogic.ts" />
/// <reference path="../abra/rest/filterOperator.ts" />
/// <reference path="../abra/rest/filter.ts" />

describe("Param Composer tests", () => {
    var connection = <Abra.Rest.IConnection>{
        connectorUrl: "https://portal.abra.eu/api/rest/testapi:data"
    };

    it("withFields array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFields(["firstname", "lastname"]);

        expect("?fields=firstname%2Clastname").toEqual(composer.compose());
    });

    it("withFields string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFields("firstname,lastname");

        expect("?fields=firstname%2Clastname").toEqual(composer.compose());
    });

    it("withAddFields array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withAddFields(["DocumentNumber", "NotPaidAmount", "PaidStatus"]);

        expect("?addfields=DocumentNumber%2CNotPaidAmount%2CPaidStatus").toEqual(composer.compose());
    });

    it("withAddFields string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withAddFields("DocumentNumber,NotPaidAmount,PaidStatus");

        expect("?addfields=DocumentNumber%2CNotPaidAmount%2CPaidStatus").toEqual(composer.compose());
    });

    it("withExpand", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"]);

        expect("?expandfields=electronicaddress_id%2Cfirmoffices").toEqual(composer.compose());
    });

    it("withExpand businessObject", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withExpand(["electronicaddress_id", "firmoffices"])
            .withExpandBo("eu.abra.ns.gx.cmpdef.BOFirmOffice", "address_id");

        var actual = "?expandfields=electronicaddress_id%2Cfirmoffices&expandfields(eu.abra.ns.gx.cmpdef.BOFirmOffice)=address_id";
        expect(actual).toEqual(composer.compose());
    });

    it("withCount", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withCount(25);

        expect("?count=25").toEqual(composer.compose());
    });

    it("withSkip", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withSkip(101);

        expect("?skip=101").toEqual(composer.compose());
    });

    it("withOrder ASC", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withOrder("name");

        expect("?order=name%3AASC").toEqual(composer.compose());
    });

    it("withOrder DESC", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withOrder(["name", "price"], Abra.Rest.SortDirection.desc);

        expect("?order=name%2Cprice%3ADESC").toEqual(composer.compose());
    });

    it("withFilter caseInsensitive", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "abra*", Abra.Rest.FilterOperator.like)
                ])
            );

        expect('?filter=%7B%22name%3Alike%3Auc%22%3A%22ABRA*%22%7D').toEqual(composer.compose());
    });

    it("withFilter caseSensitive", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "Abraham*", Abra.Rest.FilterOperator.like, true)
                ])
            );

        expect('?filter=%7B%22name%3Alike%22%3A%22Abraham*%22%7D').toEqual(composer.compose());
    });

    it("withFilter composite", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                    new Abra.Rest.FilterItem("code", "2*", Abra.Rest.FilterOperator.like),
                    new Abra.Rest.FilterItem("price", 10.2, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect('?filter=%7B%22name%3Alike%3Auc%22%3A%22ABRA*%22%2C%22code%3Alike%3Auc%22%3A%222*%22%2C%22price%3Aeq%22%3A10.2%7D')
            .toEqual(composer.compose());
    });

    it("withFilter boolean", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("hidden", false, Abra.Rest.FilterOperator.eq),
                    new Abra.Rest.FilterItem("visible", true, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect('?filter=%7B%22hidden%3Aeq%22%3Afalse%2C%22visible%3Aeq%22%3Atrue%7D').toEqual(composer.compose());
    });

    it("withFilter number", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("price", 10.5, Abra.Rest.FilterOperator.eq)
                ])
            );

        expect('?filter=%7B%22price%3Aeq%22%3A10.5%7D').toEqual(composer.compose());
    });

    it("withFilter OR", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
            );

        expect('?filter=%7B%22%3Atype%22%3A%22OR%22%2C%22name%3Alike%3Auc%22%3A%22ABRA*%22%2C%22number%3Alt%22%3A10%7D')
            .toEqual(composer.compose());
    });

    it("withFilter innerFilter", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
                .withFilters([new Abra.Rest.Filter()
                    .withFilterItems([
                        new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                        new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                    ])
                ])
            );

        expect('?filter=%7B%22%3Atype%22%3A%22OR%22%2C%22name%3Alike%3Auc%22%3A%22ABRA*%22%2C%22number%3Alt%22%3A10%2C%22%3A%22%3A%7B%22ID%3Aeq%3Auc%22%3A%221000000101%22%2C%22ObjVersion%3Alt%22%3A1%7D%7D')
            .toEqual(composer.compose());
    });

    it("withFilter innerFilters", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([
                    new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like),
                    new Abra.Rest.FilterItem("number", 10, Abra.Rest.FilterOperator.lt)
                ])
                .withFilters([new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                    .withFilters([
                        new Abra.Rest.Filter()
                            .withFilterItems([
                                new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                                new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                            ]),
                        new Abra.Rest.Filter()
                            .withFilterItems([
                                new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                                new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                            ])
                    ])
                ])
            );

        var expectData = '?filter=%7B%22%3Atype%22%3A%22OR%22%2C%22name%3Alike%3Auc%22%3A%22ABRA*%22%2C%22number%3Alt%22%3A10%2C%22%3A%22%3A%7B%22%3Atype%22%3A%22OR%22%2C%22%3A%22%3A%7B%22ID%3Aeq%3Auc%22%3A%221000000101%22%2C%22ObjVersion%3Alt%22%3A1%7D%2C%22%3A%22%3A%7B%22ID%3Aeq%3Auc%22%3A%221000000102%22%2C%22ObjVersion%3Alt%22%3A2%7D%7D%7D';
        expect(expectData).toEqual(composer.compose());
    });

    it("withFilter innerFilters2", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilters([
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000101", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 1, Abra.Rest.FilterOperator.lt)
                        ]),
                    new Abra.Rest.Filter()
                        .withFilterItems([
                            new Abra.Rest.FilterItem("ID", "1000000102", Abra.Rest.FilterOperator.eq),
                            new Abra.Rest.FilterItem("ObjVersion", 2, Abra.Rest.FilterOperator.lt)
                        ])
                ])
        );

        var expectData = '?filter=%7B%22%3Atype%22%3A%22OR%22%2C%22%3A%22%3A%7B%22ID%3Aeq%3Auc%22%3A%221000000101%22%2C%22ObjVersion%3Alt%22%3A1%7D%2C%22%3A%22%3A%7B%22ID%3Aeq%3Auc%22%3A%221000000102%22%2C%22ObjVersion%3Alt%22%3A2%7D%7D';
        expect(expectData).toEqual(composer.compose());
    });

    it("withFilter emptyFilter", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter(Abra.Rest.FilterLogic.or)
                .withFilterItems([])
            );

        expect('?').toEqual(composer.compose());
    });

    it("aggregate withSum, groupBy", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFields("amount,localamount,currency_id.code")
            .aggregate.withSum()
            .withGroupBy("currency_id");

        expect("$sum?fields=amount%2Clocalamount%2Ccurrency_id.code&groupby=currency_id").toEqual(composer.compose());
    });

    it("aggregate withCount", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([new Abra.Rest.FilterItem("name", "ABRA*", Abra.Rest.FilterOperator.like)]))
            .aggregate.withCount();

        expect('$count?filter=%7B%22name%3Alike%3Auc%22%3A%22ABRA*%22%7D').toEqual(composer.compose());
    });

    it("withFilterOperator in array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", ["001","002","003"], Abra.Rest.FilterOperator.in)
                ])
            );

        expect('?filter=%7B%22code%3Ain%22%3A%5B%22001%22%2C%22002%22%2C%22003%22%5D%7D').toEqual(composer.compose());
    });

    it("withFilterOperator in string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.in)
                ])
            );

        expect('?filter=%7B%22code%3Ain%22%3A%5B%22001%22%5D%7D').toEqual(composer.compose());
    });

    it("withFilterOperator in number", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.in)
                ])
            );

        expect('?filter=%7B%22code%3Ain%22%3A%5B5%5D%7D').toEqual(composer.compose());
    });

    it("withFilterOperator nin array", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", ["001", "002", "003"], Abra.Rest.FilterOperator.nin)
                ])
            );

        expect('?filter=%7B%22code%3Anin%22%3A%5B%22001%22%2C%22002%22%2C%22003%22%5D%7D').toEqual(composer.compose());
    });

    it("withFilterOperator nin string", () => {
        var client = new Abra.Rest.Client(connection);

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", "001", Abra.Rest.FilterOperator.nin)
                ])
            );

        expect('?filter=%7B%22code%3Anin%22%3A%5B%22001%22%5D%7D').toEqual(composer.compose());
    });

    it("withFilterOperator nin number", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
                ])
            );

        expect('?filter=%7B%22code%3Anin%22%3A%5B5%5D%7D').toEqual(composer.compose());
    });

    it("withFulltext", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFulltext("abrakadabra");

        expect('?fulltext=abrakadabra').toEqual(composer.compose());
    });

    it("with filter and fulltext", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem("code", 5, Abra.Rest.FilterOperator.nin)
                ])
            )
            .withFulltext("abrakadabra");

        expect('?filter=%7B%22code%3Anin%22%3A%5B5%5D%7D&fulltext=abrakadabra').toEqual(composer.compose());
    });

    it("with filter without FilterOperator", () => {
        var client = new Abra.Rest.Client(connection);
        var route = client.getRoute("firms");

        var composer = new Abra.Rest.ParamComposer()
            .withFilter(new Abra.Rest.Filter()
                .withFilterItems([
                    new Abra.Rest.FilterItem(":type", "OR", Abra.Rest.FilterOperator.none),
                    new Abra.Rest.FilterItem("firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
                    new Abra.Rest.FilterItem("firm_id.firm_id", "Y4I0100101", Abra.Rest.FilterOperator.none),
                ])
            );

        expect('?filter=%7B%22%3Atype%22%3A%22OR%22%2C%22firm_id%22%3A%22Y4I0100101%22%2C%22firm_id.firm_id%22%3A%22Y4I0100101%22%7D')
            .toEqual(composer.compose());
    });
});