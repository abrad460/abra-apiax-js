﻿/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkID=513275&clcid=0x409
*/
module.exports = function (grunt) {
    grunt.initConfig({
        typescript: {
            publish: {
                src: ['Scripts/abra/**/*.ts'],
                dest: 'publish/abraRestClient.js',
                options: {
                    module: 'amd', //or commonjs
                    target: 'es5', //or es3
                    sourceMap: true,
                    declaration: true
                }
            },
            paramComposer: {
                src: [
                    'Scripts/abra/**/*.ts',
                    '!Scripts/abra/rest/client.ts',
                    '!Scripts/abra/rest/connection.ts'
                ],
                dest: 'publish/abraRestParamComposer.js',
                options: {
                    module: 'amd', //or commonjs
                    target: 'es5', //or es3
                    sourceMap: true,
                    declaration: true
                }
            },
            tests: {
                src: [
                    'Scripts/tests/**/*.ts'
                ],
                dest: 'publish/tests/tests.js',
                options: {
                    module: 'amd', //or commonjs
                    target: 'es5', //or es3
                    sourceMap: true,
                    declaration: true
                }
            },
            options: {
                generateTsConfig: true
            }
        },

        // Watch
        watch: {
            ts: {
                files: [
                    'Scripts/abra/**/*.ts'
                ],
                tasks: ['typescript'],
                options: {
                    spawn: false
                }
            },
            tsTests: {
                files: [
                    'Scripts/tests/**/*.ts'
                ],
                tasks: ['typescript'],//FIXME: how to configure "subtask"?
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks("grunt-typescript");
    grunt.loadNpmTasks('grunt-contrib-watch');
};