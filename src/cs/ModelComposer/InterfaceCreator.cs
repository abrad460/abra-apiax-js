﻿using System;
using System.Text;
using System.Linq;
using ModelComposer.Models;
using System.Collections.Generic;

namespace ModelComposer
{
    static class InterfaceCreator
    {
        internal static void CreateFile(Bo bo, string fileName, Dictionary<string, string> types, string moduleName)
        {
            System.IO.File.WriteAllText(fileName, ComposeInterface(bo, types, moduleName), Encoding.UTF8);
        }

        static string ComposeInterface(Bo bo, Dictionary<string, string> types, string moduleName)
        {
            var space = "    ";

            var sb = new StringBuilder();
            sb.AppendLine("module " + moduleName + " {");
            sb.AppendLine(space + "export interface " + bo.Tablename + " {");

            // properties
            bo.Fields.ToList().ForEach(field =>
            {
                sb.AppendLine(space + space + "/**");
                sb.AppendLine(String.Concat(space + space + " * ", field.Label));

                if (!String.IsNullOrEmpty(field.Hint) && field.Label != field.Hint)
                    sb.AppendLine(String.Concat(space + space + " * ", field.Hint));

                sb.AppendLine(String.Concat(space + space + " * readonly: ", field.Readonly));
                sb.AppendLine(space + space + " */");
                sb.AppendLine(String.Concat(space + space + field.Name.ToLower(), ": ", ComposeType(types, field.Datatype), ";\r\n"));
            });

            sb.AppendLine(space + "}");
            sb.AppendLine("}");

            return sb.ToString();
        }

        static string ComposeType(Dictionary<string, string> types, string axType)
        {
            var any = "any";

            if (String.IsNullOrEmpty(axType))
                return any;

            if (!types.ContainsKey(axType))
                return any;

            return types[axType];
        }
    }
}
