﻿using Newtonsoft.Json;

namespace ModelComposer.Models
{
    public class Bo
    {
        [JsonProperty("singularname")]
        public string Singularname { get; set; }

        [JsonProperty("tablename")]
        public string Tablename { get; set; }

        [JsonProperty("fields")]
        public BoField[] Fields { get; set; }
    }
}


