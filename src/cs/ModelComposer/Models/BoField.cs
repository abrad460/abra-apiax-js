﻿using Newtonsoft.Json;

namespace ModelComposer.Models
{
    public class BoField
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("hint")]
        public string Hint { get; set; }

        [JsonProperty("datatype")]
        public string Datatype { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("persistent")]
        public bool Persistent { get; set; }

        [JsonProperty("readonly")]
        public bool Readonly { get; set; }
    }
}
