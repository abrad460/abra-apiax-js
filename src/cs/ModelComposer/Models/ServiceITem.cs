﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelComposer.Models
{
    class ServiceItem
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("businessObjectClass")]
        public string BusinessObjectClass { get; set; }
    }
}
