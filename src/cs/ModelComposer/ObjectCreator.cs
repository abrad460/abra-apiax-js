﻿using System;
using System.Text;
using System.Linq;
using ModelComposer.Models;
using System.Collections.Generic;

namespace ModelComposer
{
    static class ObjectCreator
    {
        internal static void CreateFile(Bo bo, string fileName, Dictionary<string, string> types, string moduleName, ObjectKind kind)
        {
            System.IO.File.WriteAllText(fileName, Compose(bo, types, moduleName, kind), Encoding.UTF8);
        }

        static string Compose(Bo bo, Dictionary<string, string> types, string moduleName, ObjectKind kind)
        {
            var space = "    ";

            var sb = new StringBuilder();
            sb.AppendLine("module " + moduleName + " {");
            sb.AppendLine(space + "export " + (kind == ObjectKind.Class ? "class " : "interface ") + bo.Tablename + " {");
            if (kind == ObjectKind.Class)
            {
                sb.AppendLine(space + space + "constructor() {");
                sb.AppendLine(space + space + "}\r\n");
            }

            // properties
            bo.Fields.ToList().ForEach(field =>
            {
                sb.AppendLine(space + space + "/**");
                sb.AppendLine(String.Concat(space + space + " * ", field.Label));

                if (!String.IsNullOrEmpty(field.Hint) && field.Label != field.Hint)
                    sb.AppendLine(String.Concat(space + space + " * ", field.Hint));

                sb.AppendLine(String.Concat(space + space + " * readonly: ", field.Readonly));
                sb.AppendLine(space + space + " */");
                sb.AppendLine(String.Concat(space + space + (kind == ObjectKind.Class ? "public " : String.Empty), 
                    field.Name.ToLower(), 
                    ": ", 
                    ComposeType(types, field.Datatype), 
                    ";\r\n"));
            });

            sb.AppendLine(space + "}");
            sb.AppendLine("}");

            return sb.ToString();
        }

        static string ComposeType(Dictionary<string, string> types, string axType)
        {
            var any = "any";

            if (String.IsNullOrEmpty(axType))
                return any;

            if (!types.ContainsKey(axType))
                return any;

            return types[axType];
        }
    }
}
